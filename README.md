# Ansible Debian role to install wkhtmltopdf

Due to [ERPNext](https://www.erpnext.org/) errors with the Debian packaged version of wkhtmltopdf, this role will install from the [wkhtmltopdf site](https://wkhtmltopdf.org/).

The variables in the `defaults/main.yml` file for this role might need overriding using newer values from the [wkhtmltopdf downloads page](https://wkhtmltopdf.org/downloads.html):

```yml
wkhtmltopdf_deb: wkhtmltox_0.12.5-1.stretch_amd64.deb
wkhtmltopdf_checksums: https://downloads.wkhtmltopdf.org/0.12/0.12.5/SHA256SUMS
wkhtmltopdf_deb_url: https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox_0.12.5-1.stretch_amd64.deb
```

To use this role you need to use Ansible Galaxy to install it into another repository under `galaxy/roles/wkhtmltopdf` by adding a `requirements.yml` file in that repo that contains:

```yml
- name: wkhtmltopdf
  src: https://git.coop/webarch/wkhtmltopdf.git
  version: master
  scm: git
```

And a `ansible.cfg` that contains:

```
[defaults]
retry_files_enabled = False
pipelining = True
inventory = hosts.yml
roles_path = galaxy/roles

```

And a `.gitignore` containing:

```
roles/galaxy
```

To pull this repo in run:

```bash
ansible-galaxy install -r requirements.yml --force 
```

